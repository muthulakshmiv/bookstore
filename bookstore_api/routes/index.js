var express = require('express');
var router = express.Router();

var booksController = require('../app/controllers/book-controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/categories', booksController.getCategories);
router.get('/api/booksbycategory', booksController.getBooksByCategory);
router.get('/api/booksbyid', booksController.getBookById);

module.exports = router;
