///-------------------------------------------------------------------------------------------------------------------
///   File:                 book-controller.js
///   Description:          
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------

var mongoose = require('../models/db');
var Category = require('../models/category');
var Book = require('../models/book');

//Retrieve the list of Categories
module.exports.getCategories = function (req, res) {
    var query = Category.find();
    
    query.exec(function (err, categories) {
        if (err) {
            console.log(err);            
            res.status(404).json({categories:null});            
        }        
        res.status(200).json({categories:categories});
    })
}

//Retrieve the list of Books for the specified category
module.exports.getBooksByCategory = function (req, res) {
    var query = Book.find();
    query.where({category_id: req.query.categoryId})
    query.exec(function (err, books) {
        if (err) {
            console.log(err);            
            res.status(404).json({books:null});            
        }        
        res.status(200).json({books:books});
    })
}

module.exports.getBookById = function (req, res) {
    var query = Book.find();
    query.where({_id: req.query.bookId})
    query.exec(function (err, book) {
        if (err) {
            console.log(err);            
            res.status(404).json({book:null});            
        }        
        res.status(200).json({book:book});
    })
}