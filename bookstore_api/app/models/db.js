///-------------------------------------------------------------------------------------------------------------------
///   File:                 db.js
///   Description:          File which has all the code related to database connection handling
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------

//Define database connection strings and open mongoose connection
var mongoose = require ('mongoose');

var gracefulShutdown;
//Local MongoDB URL
var dbURI = 'mongodb://localhost:27017/bookstore_demo';

//Mongolab Prod URL
//var dbURI = 'mongodb://admin:bookstore@ds035826.mlab.com:35826/bookstore_demo';

var connection = mongoose.createConnection(dbURI);

mongoose.connect(dbURI);

//Listen for mongoose connection events and output statuses to console
mongoose.connection.on('connected', function(){       
        console.log('Mongoose connected to ' + dbURI);        
});

mongoose.connection.on('error', function(err){
        console.log('Mongoose connection error : ' + err);
});

//Reusable function to close mongoose connection
gracefulShutdown = function(msg, callback){
        mongoose.connection.close(function(){
                console.log('Mongoose disconnected through ' + msg);
                callback();
        });
};

//Listen to Node processes for termination or restart signals and call gracefulShutdown function when appropriate

//For nodemon restarts
process.on('SIGUSR2', function(){
        gracefulShutdown('nodemon restart', function(){
                process.kill(process.pid, 'SIGUSR2');
        });
});

//For app termination
process.on('SIGINT', function(){
        gracefulShutdown('app termination', function(){
                process.exit(0);
        });
});

//For Heroku app termination
process.on('SIGTERM', function(){
        gracefulShutdown('Heroku app shutdown', function(){
                process.exit(0);
        });
});

module.exports = mongoose;