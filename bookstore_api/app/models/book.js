///-------------------------------------------------------------------------------------------------------------------
///   File:                 book.js
///   Description:          Defines the book schema 
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------


var mongoose = require ('mongoose');

//Defining the schema for book info
var bookSchema = new mongoose.Schema({
    _id:{
        type: Number,
        unique: true,
        required: true 
    },
    name: {
        type: String,
        trim: true,
        unique: true,
        required: true      
    },
    description: {
        type: String,
        trim: true
    },
    meta_description: {
        type: String,
        trim: true           
    },
    meta_keywords: {
        type: String,
        trim: true           
    },
    category_id: {
        type: Number,
        ref: 'Category'           
    },
    price: {
        type: Number                
    },
    sale_price: {
        type: Number                
    },
    pages: {
        type: Number                
    },
    sku:{
        type: String
    },
    author: {
        type: String                
    },
    isbn: {
        type: String                
    },
    publisher: {
        type: String                
    },
    published_year: {
        type: Number                
    },
    image_url:{
        type: String
    }
});

var Book = mongoose.model('Book', bookSchema, 'Books');
module.exports = Book;