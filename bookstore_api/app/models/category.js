///-------------------------------------------------------------------------------------------------------------------
///   File:                 category.js
///   Description:          Defines the category schema 
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------


var mongoose = require ('mongoose');

//Defining the schema for category
var categorySchema = new mongoose.Schema({
    _id:{
        type: Number,
        unique: true,
        required: true 
    },
    name: {
        type: String,
        trim: true,
        unique: true,
        required: true      
    },
    description: {
        type: String,
        trim: true
    },
    meta_description: {
        type: String,
        trim: true           
    },
    meta_keywords: {
        type: String,
        trim: true           
    }
});

var Category = mongoose.model('Category', categorySchema, 'Categories');
module.exports = Category;