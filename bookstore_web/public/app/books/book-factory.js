///-------------------------------------------------------------------------------------------------------------------
///   File:                 product-factory.js
///   Description:          The file which has the http calls to manage the category/products information
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------
bookStore.factory('BooksService',function($http,$q,apiUrl){
    return{
        getCategories:function(){
                var deferred = $q.defer();                             
                try {
                    $http.get(apiUrl + 'api/categories').success(function (data, status, headers, config) {                        
                        deferred.resolve(data);
                    }).error(function (data, status, headers, config) {                       
                        deferred.reject(data);
                    }
                    );
                }
                catch (ex) {
                    deferred.reject(ex.message);
                }
                return deferred.promise;
        },
        getBooksByCategory:function(categoryId){
                var deferred = $q.defer();                             
                try {
                    $http.get(apiUrl + 'api/booksbycategory?categoryId=' + categoryId).success(function (data, status, headers, config) {                        
                        deferred.resolve(data);
                    }).error(function (data, status, headers, config) {                       
                        deferred.reject(data);
                    }
                    );
                }
                catch (ex) {
                    deferred.reject(ex.message);
                }
                return deferred.promise;
        },
         getBookById:function(bookId){
                var deferred = $q.defer();                             
                try {
                    $http.get(apiUrl + 'api/booksbyid?bookId=' + bookId).success(function (data, status, headers, config) {                        
                        deferred.resolve(data);
                    }).error(function (data, status, headers, config) {                       
                        deferred.reject(data);
                    }
                    );
                }
                catch (ex) {
                    deferred.reject(ex.message);
                }
                return deferred.promise;
        }
    }
});