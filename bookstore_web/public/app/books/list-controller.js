///-------------------------------------------------------------------------------------------------------------------
///   File:                list-controller.js
///   Description:           
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------
bookStore.controller('listController', function($scope, $location, $sessionStorage, BooksService){
    
    BooksService.getCategories().then(function(data){
        $scope.Categories = data.categories;     
        //By default load the books in the first category
        $scope.getBooksByCategory(1);   
    });

    $scope.getBooksByCategory = function(categoryId){
        BooksService.getBooksByCategory(categoryId).then(function(data){
            $scope.Books = data.books;    
        }); 
    }

    //Add 1 quantity of the specified item in the cart (sessionstorage) 
    $scope.addToCart = function(bookId, bookName, price){
    	var cartItems;
    	if($sessionStorage.cartItems)
    		cartItems = $sessionStorage.cartItems;
    	else
    		cartItems = [];

    	//Add the book to the cart
    	var cartItem = {
    		bookId: bookId,
    		bookName: bookName,
    		price: price,
    		qty: 1,
    		total: price //Since we are adding only one quantity here
    	};
    	cartItems.push(cartItem);
    	$sessionStorage.cartItems = cartItems;
        //console.log($sessionStorage.cartItems);
        alert('Book has been successfully added!');
    }
});