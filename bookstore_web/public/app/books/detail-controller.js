///-------------------------------------------------------------------------------------------------------------------
///   File:                detail-controller.js
///   Description:           
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------
bookStore.controller('detailController', function($scope, $location, $routeParams, $sessionStorage, BooksService){
    
    BooksService.getBookById($routeParams.bookId).then(function(data){
        $scope.Book = data.book[0];    
    });

    //Add 1 quantity of the specified item in the cart (sessionstorage) 
    $scope.addToCart = function(bookId, bookName, price){
    	var cartItems;
    	if($sessionStorage.cartItems)
    		cartItems = $sessionStorage.cartItems;
    	else
    		cartItems = [];

    	//Add the book to the cart
    	var cartItem = {
    		bookId: bookId,
    		bookName: bookName,
    		price: price,
    		qty: $scope.quantity,
    		total: price //Since we are adding only one quantity here
    	};
    	cartItems.push(cartItem);
    	$sessionStorage.cartItems = cartItems;

        alert('Book has been successfully added!');
    }
});