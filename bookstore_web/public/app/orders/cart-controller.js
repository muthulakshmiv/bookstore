///-------------------------------------------------------------------------------------------------------------------
///   File:                cart-controller.js
///   Description:           
///   Author:               People10Dev                    Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------
bookStore.controller('cartController', function($scope, $location, $sessionStorage){
    
//TODO - Retrieve the values from session storage and show it in the UI
$scope.orderTotal = 0.00;

$scope.showCart = function(){
	$scope.cartItems = [];
	if($sessionStorage.cartItems)
	{
		$scope.cartItems = $sessionStorage.cartItems;
		//calculating order total
		var sTotal = 0;
		angular.forEach($scope.cartItems, function(cartItem){
			sTotal = sTotal + cartItem.price;
		});
		$scope.orderTotal = sTotal;
	}
}

$scope.showCart();

$scope.removeCartItem = function(itemIndex){
	$scope.cartItems.splice(itemIndex,1);
	$sessionStorage.cartItems = $scope.cartItems;
	//Re-calculating order total
	var sTotal = 0;
	angular.forEach($scope.cartItems, function(cartItem){
		sTotal = sTotal + cartItem.price;
	});
	$scope.orderTotal = sTotal;
	alert('Item has been successfully removed from the cart!');
}

});