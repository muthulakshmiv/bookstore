///-------------------------------------------------------------------------------------------------------------------
///   File:                 app.js
///   Description:          The file which has all the configuration for the angular js module.
///   Author:               People10Dev                   Date: 21-Sep-2016
///   Revision History:
///   Name:                 Date:               Description
///   People10Dev          21-Sep-2016         Initial Draft
///--------------------------------------------------------------------------------------------------------------------

var bookStore = angular.module('bookStore',['ngRoute', 'ngStorage']);
bookStore.constant('apiUrl','http://localhost:5000/');

bookStore.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
            .when('/', {
                    templateUrl: 'public/app/home.html' 
            })
            .when('/list', {
                    templateUrl: 'public/app/books/list.html',
                    controller: 'listController'   
            })
            .when('/detail', {
                    templateUrl: 'public/app/books/detail.html',
                    controller: 'detailController'
             })
            .when('/cart', {
                    templateUrl: 'public/app/orders/cart.html',
                    controller: 'cartController'
             })
            .when('/confirmation', {
                    templateUrl: 'public/app/orders/confirmation.html' 
            })
            .otherwise({
                    redirectTo: '/'
            });
 }]);


bookStore.config(['$httpProvider', function($httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};    
    }    
    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);